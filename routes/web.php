<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// dashboard route
Route::get('/', 'AppController@index');

/** routes for render a view for each algorithm **/
Route::get('/sort', function(){ 
	return view('result', ['algorithm' => 'Sort (PHP native)', 'url' => 'api/sort']); 
});
Route::get('/counting', function(){ 
	return view('result', ['algorithm' => 'Counting Sort', 'url' => 'api/counting']); 
});
Route::get('/quick', function(){ 
	return view('result', ['algorithm' => 'Quick Sort', 'url' => 'api/quick']); 
});
Route::get('/comb', function(){ 
	return view('result', ['algorithm' => 'Comb Sort', 'url' => 'api/comb']);  
});
Route::get('/merge', function(){ 
	return view('result', ['algorithm' => 'Merge Sort', 'url' => 'api/merge']); 
});
Route::get('/shell', function(){ 
	return view('result', ['algorithm' => 'Shell Sort', 'url' => 'api/shell']); 
});
Route::get('/insert', function(){ 
	return view('result', ['algorithm' => 'Insert Sort', 'url' => 'api/insert']); 
});
Route::get('/bubble', function(){ 
	return view('result', ['algorithm' => 'Bubble Sort', 'url' => 'api/bubble']);  
});


/** routes for api to get the times for each algorithm **/
Route::get('/api/sort', 'AppController@sort');
Route::get('/api/counting', 'AppController@countingSort');
Route::get('/api/quick', 'AppController@quickSort');
Route::get('/api/comb', 'AppController@combSort');
Route::get('/api/merge', 'AppController@mergeSort');
Route::get('/api/shell', 'AppController@shellSort');
Route::get('/api/insert', 'AppController@insertSort');
Route::get('/api/bubble', 'AppController@bubbleSort');

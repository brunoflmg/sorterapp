<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Services\SorterService as Sorter;

class AppController extends Controller
{
    private $sizes = array(10, 100, 200, 400, 600, 800, 1000, 5000, 10000, 15000, 20000, 25000, 30000);

    /**
     * Index action
     */
    public function index()
    {
        return view('dashboard');
    }

    /**
     * Sort native action
     */
    public function sort()
    {
        return [
            'algorithm' => 'Sort (php native)',
            'results' => Sorter::run('sort', $this->sizes)
        ];
    }

    /**
     * countingSort action
     */
    public function countingSort()
    {
        return [
            'algorithm' => 'Counting Sort',
            'results' => Sorter::run('sort', $this->sizes)
        ];
    }

    /**
     * quickSort action
     */
    public function quickSort()
    {
        return [
            'algorithm' => 'Quick sort',
            'results' => Sorter::run('sort', $this->sizes)
        ];
    }

    /**
     * combSort action
     */
    public function combSort()
    {
        return [
            'algorithm' => 'Comb Sort',
            'results' => Sorter::run('combSort', $this->sizes)
        ];
    }

    /**
     * mergeSort action
     */
    public function mergeSort()
    {
        return [
            'algorithm' => 'Merge Sort',
            'results' => Sorter::run('mergeSort', $this->sizes)
        ];
    }

    /**
     * shellSort action
     */
    public function shellSort()
    {
        return [
            'algorithm' => 'Shell Sort',
            'results' => Sorter::run('shellSort', $this->sizes)
        ];
    }

    /**
     * insertSort action
     */
    public function insertSort()
    {
        return [
            'algorithm' => 'Insert Sort',
            'results' => Sorter::run('shellSort', $this->sizes)
        ];
    }

    /**
     * bubbleSort action
     */
    public function bubbleSort()
    {
        return [
            'algorithm' => 'Bubble Sort',
            'results' => Sorter::run('bubbleSort', $this->sizes)
        ];
    }
}

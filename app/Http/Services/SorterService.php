<?php

namespace App\Http\Services;

/**
 * Class SorterService
 *
 * @package App\Http\Services
 */
class SorterService
{
    /**
     * Run the sorting for a given algorithm and sizes
     *
     * @param $algorithm
     * @param $sizes
     * @return array
     */
    public static function run($algorithm, $sizes)
    {
        if ($algorithm == 'sort') {
            return self::runNativeSort($sizes);
        }

        self::verifyIfMethodExist($algorithm);

        $times = array();

        // sort the elements according the sizes informed
        foreach ($sizes as $size) {

            // fill the elements array with the current size/quantity tested
            $elements = self::fillArray($size, true);

            // get an array of elements expected as result
            $expected = $elements;
            sort($expected);

            // get the time before sorting the array
            $st = microtime(true);

            // execute the array sort
            self::$algorithm($elements);

            // get the time after sorting the array
            $times[] = microtime(true) - $st;

            self::verifyIfResultMatchExpected($elements, $expected);
        }

        return $times;
    }

    /**
     * Run the php native sort function
     *
     * @param $sizes
     * @return array
     */
    private static function runNativeSort($sizes)
    {
        $times = array();

        // sort the elements according the sizes informed
        foreach ($sizes as $size) {

            // fill the elements array with the current size/quantity tested
            $elements = self::fillArray($size, true);

            // get the time before sorting the array
            $st = microtime(true);

            sort($elements);

            // get the time after sorting the array
            $times[] = microtime(true) - $st;
        }

        return $times;
    }

    /**
     * Fill an array with numbers according with the given size
     *
     * @param $size
     * @param bool $isBigInt
     * @return array
     */
    private static function fillArray($size, $isBigInt = false)
    {
        $limit = $size;

        if ($isBigInt) {
            $limit = PHP_INT_MAX;
        }

        $a = array();
        for ($i = 0; $i < $size; $i++) {
            $a[] = rand(1, $limit);
        }

        return $a;
    }

    /**
     * Compare two vars and verify if they are the same
     *
     * @param $elements
     * @param $expected
     */
    private static function verifyIfResultMatchExpected($elements, $expected)
    {
        if ($elements != $expected) {
            throw new Exception("The sorting using $algorithm does not match with the expected result.");
        }
    }

    /**
     * Verify if a given method exist in the class
     *
     * @param $methodName
     * @throws Excetion
     */
    private static function verifyIfMethodExist($methodName)
    {
        if (!method_exists(get_class(), $methodName)) {
            throw new \Exception("The $methodName is not implemented in the sorter service.");
        }
    }

    /**
     * Function for sorting an array with counting sort algorithm.
     *
     * @param $elements
     */
    private static function countingSort(&$elements)
    {
        $k = max($elements);
        $n = count($elements);
        $temp = array();

        for ($i = 0; $i <= $k; $i++) {
            $temp[$i] = 0;
        }

        for ($i = 0; $i < $n; $i++) {
            $temp[$elements[$i]]++;
        }

        $b = 0;
        for ($j = 0;$j <= $k; $j++) {
            for ($i = 0; $i < $temp[$j]; $i++) {
                $elements[$b] = $j;
                $b++;
            }
        }
    }

    /**
     * Function for sorting an array with quick sort algorithm.
     *
     * @param $elements
     * @param int $left
     * @param int $right
     */
    private static function quickSort(&$elements, $left = 0, $right = 0)
    {
        if ($right == 0) {
            $right = count($elements) - 1;
        }

        $i = $left;
        $j = $right;
        $x = $elements[($left + $right) / 2];

        do {
            while ($elements[$i] < $x) {
                $i++;
            }

            while ($elements[$j] > $x){
                $j--;
            }

            if ($i <= $j) {
                if ($elements[$i] > $elements[$j]) {
                    list($elements[$i], $elements[$j]) = array($elements[$j], $elements[$i]);
                }
                $i++;
                $j--;
            }
        } while ($i <= $j);

        if ($i < $right) {
            self::quickSort($elements, $i, $right);
        }

        if ($j > $left) {
            self::quickSort($elements, $left, $j);
        }
    }

    /**
     * Function for sorting an array with comb sort algorithm.
     *
     * @param $elements
     */
    private static function combSort(&$elements)
    {
        $gap = $n = count($elements);
        $swapped = true;

        while ($gap > 1 || $swapped) {
            if ($gap > 1) {
                $gap = floor($gap / 1.24733);
            }

            $i = 0;
            $swapped = false;

            while ($i + $gap < $n) {
                if ($elements[$i] > $elements[$i + $gap]) {
                    list($elements[$i], $elements[$i + $gap]) = array($elements[$i + $gap], $elements[$i]);
                    if (!$swapped) {
                        $swapped = true;
                    }
                }
                $i++;
            }
        }
    }

    /**
     * Function for sorting an array with merge sort algorithm.
     *
     * @param $elements
     * @param int $first
     * @param null $last
     */
    private static function mergeSort(&$elements, $first = 0, $last = null)
    {
        if (is_null($last)) {
            $last = count($elements) - 1;
        }

        if ($first < $last) {
            self::mergeSort($elements, $first, floor(($first + $last) / 2));
            self::mergeSort($elements, floor(($first + $last) / 2) + 1, $last);

            $tmp = array();

            $middle = floor(($first + $last) / 2);
            $start = $first;
            $final = $middle + 1;

            for ($i = $first; $i <= $last; $i++) {
                if (($start <= $middle) && (($final > $last) || ($elements[$start] < $elements[$final]))) {
                    $tmp[$i] = $elements[$start];
                    $start++;
                } else {
                    $tmp[$i] = $elements[$final];
                    $final++;
                }
            }

            for ($i = $first; $i <= $last; $i++) {
                $elements[$i] = $tmp[$i];
            }
        }
    }

    /**
     * Function for sorting an array with merge sort algorithm.
     *
     * @param $elements
     */
    private static function shellSort(&$elements)
    {
        $n = count($elements);
        $d = floor($n / 2);

        while ($d > 0) {
            for ($i = 0; $i < ($n - $d); $i++) {
                $j = $i;
                while ($j >= 0 && $elements[$j] > $elements[$j + $d]) {
                    list($elements[$j], $elements[$j + $d]) = array($elements[$j + $d], $elements[$j]);
                    $j--;
                }
            }
            $d = floor($d / 2);
        }
    }

    /**
     * Function for sorting an array with insert sort algorithm.
     *
     * @param $elements
     */
    private static function insertSort(&$elements)
    {
        $n = count($elements);
        for ($i = 0; $i < ($n - 1); $i++) {
            $key = $i + 1;
            $tmp = $elements[$key];
            for ($j = ($i + 1); $j > 0; $j--) {
                if ($tmp < $elements[$j - 1]) {
                    $elements[$j] = $elements[$j - 1];
                    $key = $j - 1;
                }
            }
            $elements[$key] = $tmp;
        }
    }

    /**
     * Function for sorting an array with bubble sort algorithm.
     *
     * @param $elements
     */
    private static function bubbleSort(&$elements)
    {
        $n = count($elements);
        for ($j = 0; $j < ($n - 1); $j++) {
            for ($i = 0; $i < ($n - $j - 1); $i++) {
                if ($elements[$i] > $elements[$i + 1]) {
                    list($elements[$i], $elements[$i + 1]) = array($elements[$i + 1], $elements[$i]);
                }
            }
        }
    }
}
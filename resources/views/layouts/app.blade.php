<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sorter App</title>

        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/datepicker3.css') }}" rel="stylesheet">
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

        <!--Icons-->
        <script src="{{ asset('js/lumino.glyphs.js') }}"></script>

        <!--[if lt IE 9]>
        <script src="{{ asset('js/html5shiv.js') }}"></script>
        <script src="{{ asset('js/respond.min.js') }}"></script>
        <![endif]-->
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><span>Sorter</span>App</a>
                </div>

            </div><!-- /.container-fluid -->
        </nav>

        <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
            <ul class="nav menu">
                <li><a href="/"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
                <li><a href="/sort"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Sort (native)</a></li>
                <li><a href="/counting"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Counting Sort</a></li>
                <li><a href="/quick"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Quick Sort</a></li>
                <li><a href="/comb"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Comb Sort</a></li>
                <li><a href="/merge"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Merge Sort</a></li>
                <li><a href="/shell"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Shell Sort</a></li>
                <li><a href="/insert"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Insert Sort</a></li>
                <li><a href="/bubble"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Bubble Sort</a></li>
            </ul>
        </div><!--/.sidebar-->

        @yield('content')

        <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/chart.min.js') }}"></script>
        <script>

            !function ($) {
                $(document).on("click","ul.nav li.parent > a > span.icon", function(){
                    $(this).find('em:first').toggleClass("glyphicon-minus");
                });
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function () {
                if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
            })
            $(window).on('resize', function () {
                if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
            })
        </script>

        @yield('customjs')
        
    </body>

</html>

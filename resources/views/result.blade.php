@extends('layouts.app')

@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li class="active">{{ $algorithm }}</li>
        </ol>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ $algorithm }}</h1>
        </div>
    </div>
    <!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Algorithmic performance report</div>
                <div class="panel-body">
                    <div id="ajax-loader" style="padding: 15% 0 0 50%">
                        <img src="{{ asset('images/ajax-loader.gif') }}" />
                    </div>
                    <div class="canvas-wrapper">
                        <canvas class="main-chart" id="line-chart" height="200" width="600"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/.row-->

</div>	
<!--/.main-->

@endsection

@section('customjs')

<script>

    var func = function(){ return Math.round(Math.random()*1000)};

    window.onload = function(){

        var request = $.ajax({
            url: "/{{ $url }}",
            method: "GET"
        });

        request.done(function(response) {

            $('#ajax-loader').hide();

            var lineChartData = {
                labels : ['10', '100', '200', '400', '600', '800', '1000', '5000', '10000', '15000', '20000', '25000', '30000'],
                datasets : [
                    {
                        label: "{{ $algorithm }}",
                        fillColor : "rgba(48, 164, 255, 0.2)",
                        strokeColor : "rgba(48, 164, 255, 1)",
                        pointColor : "rgba(48, 164, 255, 1)",
                        pointStrokeColor : "#fff",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(48, 164, 255, 1)",
                        data : response.results
                    }
                ]
            }

            var chart1 = document.getElementById("line-chart").getContext("2d");
            window.myLine = new Chart(chart1).Line(lineChartData, {
                responsive: true
            });
        });

        request.fail(function(jqXHR, textStatus) {
            alert( "Request failed: " + textStatus );
        });

    };

</script>

@endsection
@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li class="active">Dashboard</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Welcome</h1>
        </div>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Guidelines</div>
                <div class="panel-body">
                    <p>This app is about the benchmark of some sorting algorithms, written on PHP. All the algorithms used are listed according to the side menu.</p>

                    <p>The following arrays dimensions, used for sorting, are presented:</p>

                    <ul><li>10</li><li>100</li><li>200</li><li>400</li><li>600</li><li>800</li><li>1000</li><li>5000</li><li>10000</li><li>15000</li><li>20000</li><li>25000</li><li>30000</li></ul>

                    <p>In each page you'll be able to see the algorithm performance report.</p>

                </div>
            </div>
        </div>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Summary</div>
                <div class="panel-body">
                    <p>
                    QuickSort is rightfully considered quite a good algorithm. CountingSort is really good at small value ranges; otherwise it can’t manage due to low memory. Bubble sort and its modifications are not applicable to practical use.
                    </p>

                    <p>While a fun exercise, use the built in sort function. Building a sort function in interpreted PHP code will never be faster than the C variant that sort() employs.</p>

                </div>
            </div>
        </div>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Final considerations</div>
                <div class="panel-body">
                    
                    <p>I tried to use something new to me, instead staying in my confort zone.</p>

                    <p>I followed this idea because every day we need to start use some new technology, API, Framework, etc. So I tried to show how flexible I am in this aspect too.</p>

                    <p>It wasn't my best exemple of project, but I think I was able to show how I do the things in a short time frame.</p>

                    <p>Unfortunately I don't have enough time to write the unit tests.</p>
                    
                    <br>

                    <p>Thank you,<br>Bruno</p>


                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

@endsection